let canvas = new fabric.Canvas('c',{
    uniScaleTransform: true});

let text = new fabric.Text('Texdsadsadsadsadsat', {
    left: 50,
    top: 50,
});
let rect2 = new fabric.Rect({
    width: 200, height: 100, left: 0, top: 50, angle: 30,
    fill: 'rgba(255,0,0,0.5)',
    left: 0,
    zIndex:5000
});

canvas.add(text);
canvas.add(rect2);
function center(){
    let obj = canvas.getActiveObject();
    let leftcenter = canvas.width / 2;
    let halfleft = (obj.width * obj.scaleX) / 2;
    obj.set("left", leftcenter - halfleft);
    obj.set("top", obj.get('top'));
    obj.setCoords();
    canvas.renderAll();
}

function right() {
    let obj = canvas.getActiveObject();
    let leftcenter = canvas.width / 1.1;
    let halfleft = obj.width / 1.1;
    obj.set("left", leftcenter);
    obj.set("top", obj.get('top'));
    obj.setCoords();
    canvas.renderAll();

}
function left() {
    let obj = canvas.getActiveObject();
    let leftcenter = canvas.width / 10;
    let halfleft = obj.width / 8;
    obj.set("left", leftcenter);
    obj.set("top", obj.get('top'));
    // obj.setCoords();
    canvas.renderAll();
}


$(function() {
    canvas.on("selection:created", function(e) {
        var activeObj = canvas.getActiveGroup();
        if(activeObj.type === "group") {
            console.log("Group created");

            var groupWidth = e.target.getWidth();
            var groupHeight = e.target.getHeight();


            e.target.forEachObject(function(obj) {
                var itemWidth = obj.getBoundingRect().width;
                var itemHeight = obj.getBoundingRect().height;

                // ================================
                // OBJECT ALIGNMENT: " H-LEFT "
                // ================================
                $('#objAlignLeft').click(function() {
                    obj.set({
                        left: -(groupWidth / 2),
                        originX: 'left'
                    });
                    obj.setCoords();
                    canvas.renderAll();
                });
                // ================================
                // OBJECT ALIGNMENT: " H-CENTER "
                // ================================
                $('#objAlignCenter').click(function() {
                    obj.set({
                        left: (0 - itemWidth/2),
                        originX: 'left'
                    });
                    obj.setCoords();
                    canvas.renderAll();
                });
                // ================================
                // OBJECT ALIGNMENT: " H-RIGHT "
                // ================================
                $('#objAlignRight').click(function() {
                    obj.set({
                        left: (groupWidth/2 - itemWidth/2),
                        originX: 'center'
                    });
                    obj.setCoords();
                    canvas.renderAll();
                });

            });

        }
    }); // END OF " SELECTION:CREATED "

    canvas.on("selection:cleared", function(e) {
        $('#objAlignLeft').off('click');
        $('#objAlignCenter').off('click');
        $('#objAlignRight').off('click');
    });

});
